ClearOS Firewall
================

Packet/connection MARK Bitmap
-----------------------------

When the Firewall Engine marks packets and/or connections, it adheres to the following address space assignments.

```
31                                                              0
_________________________________________________________________
| | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |
|       |               |               |                       |
    A           B               C                   D
 4-bits      8-bits          8-bits              12-bits


A = MultiWAN, 4-bits, 16 Interfaces.            0xF0000000 << 28
B = Bandwidth QoS, 8-bits, Up to 36 interfaces. 0x0FF00000 << 20
C = ClearOS Reserved, 8-bits.                   0x000FF000 << 12
D = User, 12-bits. Marks for up to 4,096 user defined rules
```

User/third-party Assignments
----------------------------

The lowest 12 bits are reserved for user and/or third-party applications.  The following a list of known bit assignments:
- Netify FWA uses bit 11 (0x800).